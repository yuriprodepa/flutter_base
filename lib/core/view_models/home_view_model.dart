import 'package:flutter_base/core/enums/view_state.dart';

import 'base_view_model.dart';

class HomeViewModel extends BaseViewModel {
  int _page = 1;
  int get page => _page;

  HomeViewModel() {
    _page = 1;
  }

  Future<void> setPage(int page_number) async {
    _page = page_number;
    setState(ViewState.IDLE);
  }
}
