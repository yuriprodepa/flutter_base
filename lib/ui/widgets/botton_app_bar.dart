import 'package:flutter/material.dart';
import 'package:flutter_base/core/view_models/home_view_model.dart';
import 'package:provider/provider.dart';

class CustomAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final model = Provider.of<HomeViewModel>(context);

    return Positioned(
      bottom: 0,
      child: Container(
        height: 85,
        decoration: BoxDecoration(
          border: Border(
            top: BorderSide(
              width: 4,
              color: Colors.grey[200],
            ),
          ),
        ),
        width: MediaQuery.of(context).size.width,
        child: Padding(
            padding: EdgeInsets.only(left: 16, right: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    model.setPage(1);
                  },
                  child: model.page == 1
                      ? Image.asset(
                          'assets/icons/bolinha.png',
                          height: 60,
                        )
                      : Image.asset(
                          'assets/icons/bolinha_cinza.png',
                          height: 60,
                        ),
                ),
                GestureDetector(
                  onTap: () {
                    model.setPage(2);
                  },
                  child: model.page == 2
                      ? Image.asset(
                          'assets/icons/menininha.png',
                          height: 60,
                        )
                      : Image.asset(
                          'assets/icons/menininha_cinza.png',
                          height: 60,
                        ),
                ),
                GestureDetector(
                  onTap: () {
                    model.setPage(3);
                  },
                  child: model.page == 3
                      ? Image.asset(
                          'assets/icons/escudo.png',
                          height: 60,
                        )
                      : Image.asset(
                          'assets/icons/escudo_cinza.png',
                          height: 60,
                        ),
                ),
                GestureDetector(
                  onTap: () {
                    model.setPage(4);
                  },
                  child: model.page == 4
                      ? Image.asset(
                          'assets/icons/casa.png',
                          height: 60,
                        )
                      : Image.asset(
                          'assets/icons/casa_cinza.png',
                          height: 60,
                        ),
                ),
              ],
            )),
      ),
    );
  }
}
