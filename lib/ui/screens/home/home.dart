import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/ui/values/styles.dart';
import 'package:flutter_base/ui/widgets/botton_app_bar.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: Padding(
            padding: const EdgeInsets.only(left: 14),
            child: Image.asset('assets/icons/bandeira.png'),
          ),
          actions: <Widget>[
            Expanded(
                child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 85, right: 16),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Image.asset(
                          'assets/icons/coroa.png',
                          height: 30,
                        ),
                        Text(
                          "216",
                          style: AppTheme.coroa,
                        )
                      ]),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 16, top: 2),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Image.asset(
                          'assets/icons/foguinho.png',
                          height: 30,
                        ),
                        Text(
                          "3",
                          style: AppTheme.foguinho,
                        )
                      ]),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 16, top: 2),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Image.asset(
                          'assets/icons/pedra.png',
                          height: 30,
                        ),
                        Text(
                          "588",
                          style: AppTheme.pedra,
                        )
                      ]),
                ),
              ],
            )),
          ],
        ),
        body: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            CustomAppBar(),
          ],
        ));
  }
}
